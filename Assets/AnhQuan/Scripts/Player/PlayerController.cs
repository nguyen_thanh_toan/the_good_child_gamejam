﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    protected Joystick joystick;
    Rigidbody2D rb;
    Animator animator;
    public bool isMove,isFixing,isSleep,isDead;
    public int speed = 3,dir=1;
    void Start()
    {
        joystick = GameObject.FindObjectOfType<Joystick>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        
    }

     void Update()
    {
        animator = gameObject.GetComponent<Animator>();
        animator.SetBool("isMove",isMove);
        animator.SetBool("isFixing",isFixing);
        animator.SetBool("isSleep",isSleep);
        animator.SetBool("isDead",isDead);


        Move();
        
    }

    void Move(){
        rb.velocity = new Vector2(speed * joystick.Direction.x,speed * joystick.Direction.y);
        
        if (joystick.Direction != new Vector2(0,0)){
            isMove = true;
        }
            else
            {
                isMove = false;
            }
        if (joystick.Direction.x != 0)
            if (joystick.Direction.x > 0 ){
                transform.localScale = new Vector3(-0.1f,transform.localScale.y,transform.localScale.z);
            }
            else{
                transform.localScale = new Vector3(0.1f,transform.localScale.y,transform.localScale.z);
            }
    }

}
