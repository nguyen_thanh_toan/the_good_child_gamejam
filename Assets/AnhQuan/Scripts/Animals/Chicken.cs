﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chicken : MonoBehaviour
{
    public GameObject egg;
    private bool move = false;
    private int countMove = 0;
    private float speed = 0.5f;
    private Vector3 vectorMove;
    private Animator anim;
    float randPos;
    public int timeCount=0;
    private void Start()
    {
        anim = GetComponent<Animator>();
        Move();
        SpawnEgg();
    }
    private void Update()
    {
        if (transform.position != vectorMove)
        {
            transform.position = Vector2.MoveTowards(transform.position, vectorMove, speed * Time.deltaTime);
        }
        else
        {
            if (move)
            {
                if (countMove == 2)
                {
                    move = false;
                    anim.speed = 0;
                    Invoke("Move", 2f);
                    countMove = 0;
                }
                else
                {
                    Move();
                }
            }
        }
    }

    void Move()
    {
        move = true;
        countMove++;
        anim.speed = 1;
        int randX = Random.Range(0, 2);
        if (randX == 0)
            randPos = Random.Range(transform.position.x - 0.5f, transform.position.x + 0.5f);
        else
            randPos = Random.Range(transform.position.y - 0.5f, transform.position.y + 0.5f);
        if (randX == 0)
        {
            vectorMove = new Vector2(randPos, transform.position.y);
            if (randPos < transform.position.x)
            {
                anim.SetBool("MoveDown", false);
                anim.SetBool("MoveUp", false);
                anim.SetBool("MoveRight", false);
                anim.SetBool("MoveLeft", true);
            }
            else
            {
                anim.SetBool("MoveDown", false);
                anim.SetBool("MoveUp", false);
                anim.SetBool("MoveLeft", false);
                anim.SetBool("MoveRight", true);
            }
        }
        else
        {
            vectorMove = new Vector2(transform.position.x, randPos);
            if (randPos < transform.position.y)
            {
                anim.SetBool("MoveLeft", false);
                anim.SetBool("MoveUp", false);
                anim.SetBool("MoveRight", false);
                anim.SetBool("MoveDown", true);
            }
            else
            {
                anim.SetBool("MoveDown", false);
                anim.SetBool("MoveRight", false);
                anim.SetBool("MoveLeft", false);
                anim.SetBool("MoveUp", true);
            }
        }
    }
    void SpawnEgg()
    {
        if (timeCount == 360)
            Instantiate(egg, transform.position, Quaternion.identity, GameObject.FindGameObjectWithTag("ChickenHouse").GetComponent<Transform>());
        else
        {
            timeCount++;
            Invoke("SpawnEgg", 1);
        }
    }
}
