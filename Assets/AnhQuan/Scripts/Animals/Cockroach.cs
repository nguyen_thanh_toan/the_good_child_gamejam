﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cockroach : MonoBehaviour
{
    public Vector2 moveVector2;
    private void Start()
    {
        InvokeRepeating("Move", 0, 5);
    }
    void Update()
    {
        transform.Translate(new Vector2(0, 1) * 1f * Time.deltaTime);
        Destroy(this.gameObject, 300);
    }
    void Move()
    {
        int randX = Random.Range(1, 5);
        switch(randX)
        {
            case 1:
                transform.rotation = Quaternion.Euler(0, 0, 00);
                moveVector2 = Vector2.up;
                break;
            case 2:
                transform.rotation = Quaternion.Euler(0, 0, 90);
                moveVector2 = Vector2.left;
                break;
            case 3:
                transform.rotation = Quaternion.Euler(0, 0, 180);
                moveVector2 = Vector2.down;
                break;
            case 4:
                transform.rotation = Quaternion.Euler(0, 0, -90);
                moveVector2 = Vector2.right;
                break;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Move();
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        Move();
    }
    public void DestroyCockroach()
    {
        Destroy(this.gameObject);
        PlayerPrefs.SetInt("CockroachDeadCount", PlayerPrefs.GetInt("CockroachDeadCount") + 1);
    }
}
