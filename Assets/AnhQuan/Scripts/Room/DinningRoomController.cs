﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DinningRoomController : MonoBehaviour
{
    private Animator anim;
    private Image imgFridge;
    private Button buttonFridge;
    private Button buttonPan;
    public GameObject ingredient;
    public GameObject cakeEgg;
    private Inventory inventory;
    public Slider sliderProcessCook; // thanh tiến độ nấu
    private Transform tranfPan;
    public GameObject process;
    public GameObject effect;
    float fillvalue = 0;
    GameController game;
    void Start()
    {
        anim = GameObject.FindGameObjectWithTag("Fridge").GetComponent<Animator>();
        imgFridge = GameObject.FindGameObjectWithTag("Fridge").GetComponent<Image>();
        buttonFridge = GameObject.FindGameObjectWithTag("Fridge").GetComponent<Button>();
        buttonPan = GameObject.FindGameObjectWithTag("Pan").GetComponent<Button>();
        inventory = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
        tranfPan = GameObject.FindGameObjectWithTag("Pan").GetComponent<Transform>();
        game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

    }

    public void OpenFridge()
    {
        // mở tủ lạnh
        anim.SetBool("Open", true);
        imgFridge.rectTransform.sizeDelta = new Vector2(204, 307);
        imgFridge.transform.position = new Vector3(imgFridge.transform.position.x + 0.15f, imgFridge.transform.position.y, 0f);
        Invoke("CloseFridge", 10f);
        buttonFridge.enabled = false;
        // bật các item có trong tủ lạnh lên
        ingredient.SetActive(true);
    }
    public void CloseFridge()
    {
        // đóng tủ lạnh
        anim.SetBool("Open", false);
        imgFridge.rectTransform.sizeDelta = new Vector2(128, 307);
        imgFridge.transform.position = new Vector3(imgFridge.transform.position.x - 0.15f, imgFridge.transform.position.y, 0f);
        buttonFridge.enabled = true;
        // tắt các item có trong tủ lạnh lên
        ingredient.SetActive(false);
    }
    public void Cook()
    {
        for (int i = 0; i < 2; i++)
        {
            // nếu người chơi có trứng và phô mai thì mới nấu được bánh
            if ((inventory.nameObject[i] == "Egg(Clone)" && inventory.nameObject[i + 1] == "Cheese" ) || (inventory.nameObject[i] == "Cheese" && inventory.nameObject[i + 1] == "Egg(Clone)"))
            {
                // xóa 2 vật phẩm này đi
                inventory.isFull[i] = false;
                inventory.isFull[i + 1] = false;
                // bật thanh tiến độ và lửa nấu
                process.SetActive(true);
                effect.SetActive(true);
                buttonPan.enabled = false;
                Cooking();
                break;
            }
        }
    }
    public void Cooking()
    {
        fillvalue += 0.01f;
        sliderProcessCook.value = fillvalue / 1;
        if (fillvalue < 1)
            Invoke("Cooking", 0.5f);
        else
        {
            game.missDone("Cocking");
            // khi xong tiến độ nấu thì xuất ra bánh trứng
            Instantiate(cakeEgg, new Vector3(tranfPan.position.x - 0.2f, tranfPan.position.y + 0.1f, 0), Quaternion.identity, tranfPan);
            // tắt thanh tiến trình và hiệu ứng
            process.SetActive(false);
            effect.SetActive(false);
        }
    }
}
