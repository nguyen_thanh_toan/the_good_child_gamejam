﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    private Inventory inventory;
    public GameObject itemButton;

    void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
        if (gameObject.tag == "Egg" && transform.parent.tag == "Untagged")
            transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);
    }

    // Update is called once per frame
    public void PickupObject()
    {
        if (transform.parent.tag == "IngredientFridge" || transform.parent.tag == "Pan" || transform.parent.tag == "ChickenHouse")
        {
            for (int i = 0; i < 3; i++)
            {
                if (inventory.isFull[i] == false) // nếu ô đồ chưa full thì nhặt vào
                {
                    inventory.isFull[i] = true;
                    inventory.nameObject[i] = name;
                    Instantiate(itemButton, inventory.slot[i].transform.position, Quaternion.identity, inventory.slot[i].transform);
                    Destroy(this.gameObject);
                    break;
                }
            }
        }
    }
}
