﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Chest : MonoBehaviour
{
    private Animator anim;
    private Button buttonChest;
    private Inventory inventory;
    public GameObject ingredient;
    public bool open = false;
    private void Start()
    {
        anim = GetComponent<Animator>();
        buttonChest = GetComponent<Button>();
        inventory = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
    }
    public void OpenChest()
    {
        if(open == true)
        {
            anim.SetBool("Open", true);
            buttonChest.enabled = false;
            Invoke("CloseChest", 10f);
        }
        else
        {
            for (int i = 0; i < 2; i++)
            {
                if (inventory.nameObject[i] == "Key")
                {
                    open = true;
                    anim.SetBool("Open", true);
                    buttonChest.enabled = false;
                    Invoke("CloseChest", 10f);
                    inventory.isFull[i] = false;
                    break;
                }
            }
        }
    }
    public void StopOpen()
    {
        gameObject.GetComponent<Animator>().speed = 0;
        ingredient.SetActive(true);
    }
    public void CloseChest()
    {
        anim.SetBool("Open", false);
        buttonChest.enabled = true;
        ingredient.SetActive(false);
        gameObject.GetComponent<Animator>().speed = 1;
    }
}
