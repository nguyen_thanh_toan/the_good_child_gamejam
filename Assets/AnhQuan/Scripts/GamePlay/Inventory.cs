﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public bool[] isFull;
    public string[] nameObject;
    public GameObject[] slot;
    public int[] items;
    public Transform[] slots;
    public GameObject eggDinner;
    public GameObject cheeseDinner;
    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            isFull[i] = false;
        }
    }
    private void Update()
    {
        for (int i = 0; i < 3; i++)
        {
            if (isFull[i] == false && nameObject[i] != "")
            {
                nameObject[i] = "";
                GameObject slotChild = slot[i].transform.GetChild(0).gameObject;
                Destroy(slotChild);
            }
        }
    }
}
