﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sleep : MonoBehaviour
{
    private PlayerController player;
    private Button buttonSleep;
    private Transform transfPlayer;
    GameController game;
    BoxCollider2D nv;
    private void Start()
    {
        game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        transfPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        buttonSleep = GetComponent<Button>();
        nv =  GameObject.FindGameObjectWithTag("Player").GetComponent<BoxCollider2D>();
    }
    public void Sleeppy()
    {
        player.isSleep = true;
        if (true)
        {
            nv.isTrigger = true;
            transfPlayer.position = transform.position;
            player.speed = 0;
            Invoke("BuffSpeed", 10f);
            buttonSleep.enabled = false;
        }
    }

    void BuffSpeed()
    {
        player.isSleep = false;
         nv.isTrigger = false;
        transfPlayer.position = new Vector2(transfPlayer.position.x + 1f, transfPlayer.position.y);
        player.speed = 6;
        Invoke("NeffSpeed", 20f);
        game.missDone("Sleep");
    }

    void NeffSpeed()
    {
       
        player.speed = 3;
        buttonSleep.enabled = true;
    }
}
