﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorball : MonoBehaviour
{
    private SpriteRenderer SR;
    void Start()
    {
        SR = GetComponent<SpriteRenderer>();
        SR.color = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 255);
        Destroy(this.gameObject, 4.1f);
    }
    private void Update()
    {
        transform.Translate(Vector2.left * 1f * Time.deltaTime);
    }
}
