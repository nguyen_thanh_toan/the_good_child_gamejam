﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour
{
    void Start()
    {
        Destroy(this.gameObject, 4.1f);
    }

    void Update()
    {
        transform.Translate(Vector2.left * 1f * Time.deltaTime);
    }
}
