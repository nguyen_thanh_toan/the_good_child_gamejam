﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObs : MonoBehaviour
{
    public Transform miniGame;
    public Transform top;
    public Transform bot;
    public GameObject obsTop;
    public GameObject obsBot;
    public bool spawn = true;
    private BallRunning ball;

    private void Start()
    {
        ball = GameObject.FindGameObjectWithTag("BallRunning").GetComponent<BallRunning>();
        Spawn();
    }

    void Spawn()
    {
        if (spawn)
        {
            if (Random.Range(1, 3) == 1)
                Instantiate(obsTop, top.position, Quaternion.identity, miniGame);
            else
                Instantiate(obsBot, bot.position, Quaternion.identity, miniGame);
            if (Random.Range(1, 5) == 1)
                Invoke("Spawn", 1f);
            else
                Invoke("Spawn", 2f);
        }
    }
}
