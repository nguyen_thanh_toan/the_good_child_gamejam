﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectBall : MonoBehaviour
{
    ParticleSystem ps;
    private SpriteRenderer SR;
    private void Start()
    {
        SR = GameObject.FindGameObjectWithTag("BallRunning").GetComponent<SpriteRenderer>();
    }
    private void Update()
    {
        ps = GetComponent<ParticleSystem>();
        var main = ps.main;
        main.startColor = SR.color;
    }
}
