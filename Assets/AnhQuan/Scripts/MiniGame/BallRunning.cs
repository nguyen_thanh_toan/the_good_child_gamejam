﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallRunning : MonoBehaviour
{
    public SpriteRenderer SR;
    private SpriteRenderer SRChange;
    public GameObject gameOver;
    public Text textScore;
    public Transform transfBall;
    public Transform miniGame;
    public Transform mid;
    public GameObject changeBallColor;
    Vector2 dodgeVector2;
    public float speed = 0.5f;
    bool change = true;
    public int score = 0;
    float oldTimeScore;
    GameController game;

    

    private void Start()
    {
        game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        SR = GetComponent<SpriteRenderer>();
        PlayerPrefs.SetInt("PlayMiniGame", 0);
    }
    private void Update()
    {
        if(PlayerPrefs.GetInt("PlayMiniGame") == 1)
        {
            transform.Translate(dodgeVector2 * speed * Time.deltaTime);
        textScore.text = "Score: " + score;
        if (oldTimeScore < Time.time - 1 && PlayerPrefs.GetInt("PlayMiniGame") == 1)
        {
            oldTimeScore = Time.time;
            score++;
            if (score == 30){
                game.missDone("Reach 30 point in minigame");
            }
            if (score % 10 == 0)
                SpawnBallColor();
        } 

        if (Input.GetKeyDown(KeyCode.Mouse0)){
            Dodge();
        }

        }
    }

    
    void SpawnBallColor()
    {
        Instantiate(changeBallColor, mid.position, Quaternion.identity, miniGame);
    }
    public void Dodge()
    {
        if (change)
            dodgeVector2 = Vector2.up;
        else
            dodgeVector2 = Vector2.down;
        change = !change;
    }

    public void BuffSpeed()
    {
        if (speed < 5 && speed != 0)
        {
            speed += 0.1f;
            Invoke("BuffSpeed", 1);
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("LimitMiniGame") || col.CompareTag("Obstacles"))
        {
            gameOver.SetActive(true);
            speed = 0;
            gameObject.SetActive(false);
        }
        if (col.CompareTag("ChangeBallColor"))
        {
            SRChange = col.GetComponent<SpriteRenderer>();
            SR.color = SRChange.color;
            Destroy(col.gameObject);
        }
    }
}
