﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlay : MonoBehaviour
{
    private BallRunning ball;
    private Transform transfBall;
    public GameObject miniGame;
    public GameObject ballRunning;
    public SpawnObs spawnObs;
    public GameObject []shutdownObj;

    public void NewGame()
    {
        ballRunning.SetActive(true);
        PlayerPrefs.SetInt("PlayMiniGame", 1);
        ball = GameObject.FindGameObjectWithTag("BallRunning").GetComponent<BallRunning>();
        transfBall = GameObject.FindGameObjectWithTag("BallRunning").GetComponent<Transform>();
        transfBall.position = ball.transfBall.position;
        ball.SR.color = Color.white;
        ball.score = 0;
        ball.speed = 0.5f;
        ball.Dodge();
        ball.BuffSpeed();
        gameObject.SetActive(false);
    }
    public void Exit()
    {
        ballRunning.SetActive(true);
        PlayerPrefs.SetInt("PlayMiniGame", 0);
        ball = GameObject.FindGameObjectWithTag("BallRunning").GetComponent<BallRunning>();
        transfBall = GameObject.FindGameObjectWithTag("BallRunning").GetComponent<Transform>();
        transfBall.position = ball.transfBall.position;
        ball.SR.color = Color.white;
        ball.score = 0;
        ball.speed = 0.5f;
        shut();
        miniGame.SetActive(false);
    }

     void shut(){
        for (int i = 0; i < shutdownObj.Length;i++){
            shutdownObj[i].SetActive(true);
        }
    }
}
