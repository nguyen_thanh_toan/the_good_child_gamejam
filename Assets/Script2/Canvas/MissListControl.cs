﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissListControl : MonoBehaviour
{
    Animator animator;
   bool change;

    void Start(){
        animator = gameObject.GetComponent<Animator>();
        
    }

    void Update(){
        animator.SetBool("change",change);
    }

    public void changeState(){
       change = !change;
    }
}
