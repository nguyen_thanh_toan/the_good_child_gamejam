﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
   public bool coDien=true;
   public bool chonMission=false;
   public Text []listMission;
   int missCount = 0;
   public GameObject win,lost;
   public Image missionBar;
   public Text deadText;
    public int racCount =0;
    void Start()
    {
       missCount = listMission.Length;
       

    }

    
    void Update()
    {
        
    }

    public void missDone(string s){
        missCount -= 1;
        int n = missCount ;
       
        for (int i = 0;i < n;i++){
           if (listMission[i].text == s){
                listMission[i].text = listMission[i+1].text;
                for (int j = i;j < n;j++){
                     listMission[j].text = listMission[j+1].text;
                }
           }

        }
        listMission[n].text="";

        incMissBar();
        if (n == 0){
            win.SetActive(true);
        }
    }

    public void winFun(){
        win.gameObject.SetActive(true);
    }
    void incMissBar(){
         missionBar.transform.localScale = new Vector3(missionBar.transform.localScale.x + 0.2f,missionBar.transform.localScale.y,missionBar.transform.localScale.z);
            if (missionBar.transform.localScale.x < (float)(listMission.Length-missCount-1)/listMission.Length){
                Debug.Log("a");
                incMissBar();
            }
    }
    public void Dead(){
        // Time.timeScale = 0;
        lost.SetActive(true);
      deadText.text = "Opps! Electric is dangerous! Turn it off";
       Invoke("shut",2);
    }

    void shut(){
       lost.SetActive(false);
    }
    public void nhan(){
        chonMission = true;
    }
    public void tha(){
        chonMission = false;
    }

}
