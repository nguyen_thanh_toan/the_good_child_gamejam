﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nhatchoi : MonoBehaviour
{
    private Inventory inventory;
    public GameObject itemButton;
    public GameObject effect;
    private void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player")) {
            // spawn the sun button at the first available inventory slot ! 
            for (int i = 0; i < inventory.items.Length; i++)
            {
                if (inventory.items[i] == 0) { // check whether the slot is EMPTY
                    Instantiate(effect, transform.position, Quaternion.identity);
                    Instantiate(itemButton, inventory.slots[i].position, Quaternion.identity, inventory.slots[i]);
                    inventory.items[i] = 1; // makes sure that the slot is now considered FULL
                    Destroy(this.gameObject);
                    if(gameObject.tag == "choi")
                        PlayerPrefs.SetInt("CoChoi", 1);
                    if(gameObject.tag == "co")
                        PlayerPrefs.SetInt("CoCo", 1);
                    break;
                }
            }
        }
        
    }
}

