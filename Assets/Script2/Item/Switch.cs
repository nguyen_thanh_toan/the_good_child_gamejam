﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Switch : MonoBehaviour
{
    GameController game;
    bool vao = false;
    public Sprite sprite1,sprite2;
    void Start()
    {
         game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (game.coDien){
            gameObject.GetComponent<Image>().sprite = sprite1;

        }
        else{
            gameObject.GetComponent<Image>().sprite = sprite2;
        }
    }

    public void batTat(){
        game.coDien = !game.coDien;
    }

    void OnCollisionEnter2D(Collision2D col){
        if (col.collider.tag == "Player"){
            vao = true;
        
        }
    }
    void OnCollisionExit2D(Collision2D col){
        if (col.collider.tag == "Player"){
            vao = false;
        }
    }
}
