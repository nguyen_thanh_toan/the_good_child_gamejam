﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class clock : MonoBehaviour
{
    private TMP_FontAsset textTimer;
    GameController game;
    private float timer = 360.0f;
    private bool isTimer = false;
    public Text textClock;
     int minutes;
     int seconds;

     void Start(){
         game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
         isTimer = true;
     }
    void Update()
    {
        if(isTimer)
        {
            timer += Time.deltaTime;
            DisplayTime();
            CheckDisplayTime();
        }
    }

    void DisplayTime()
    {
        
         minutes = Mathf.FloorToInt(timer / 60f);
         seconds = Mathf.FloorToInt(timer - minutes * 60);
        textClock.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }


    public void CheckDisplayTime()
    {
    if(minutes == 18 && seconds == 00)
        {
            game.winFun();
            isTimer = false;
        }
    }
        
}