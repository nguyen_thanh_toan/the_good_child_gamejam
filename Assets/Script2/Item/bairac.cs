﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bairac : MonoBehaviour
{
    int count = 0;
    GameController game;

    void Start(){
        game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
    public void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Player") && PlayerPrefs.GetInt("CoChoi") == 1)
            {
                game.racCount++;
                if (game.racCount == 4){
                    game.missDone("Clean");
        }
                Destroy(this.gameObject);
            }
    }
}