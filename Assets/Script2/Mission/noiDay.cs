﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class noiDay : MonoBehaviour
{

    public Image day1, day2;
    public GameObject panel;
    public int zone = 100;
    GameController game;
    bool vao = false;
    PlayerController player;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }


    void Update()
    {
        if (Input.touchCount >= 2)
        {
            if (panel.activeSelf)
                if ((checkTouch(Input.touches[0].position) && checkTouch(Input.touches[1].position)) && !game.coDien)
                {
                    if (!game.coDien)
                    {
                        if (day1.transform.localScale.x < 5)
                        {
                            player.isFixing = true;
                            day1.transform.localScale = new Vector3(day1.transform.localScale.x + 0.2f, day1.transform.localScale.y, day1.transform.localScale.z);
                        }
                        else
                        {
                            player.isFixing = false;
                            panel.SetActive(!panel.activeSelf);
                            game.missDone("Fix wire electric");
                        }
                    }
                    else
                    {
                        game.Dead();
                    }

                }

        }

        // if (vao  && game.chonMission ){
        //     panel.SetActive(true);
        // }

    }

    public void active()
    {
        panel.SetActive(true);
    }

    public bool checkTouch(Vector2 touch)
    {
        if (touch.x < (day2.transform.position.x - 200))
        {
            //  Vector3 temp = Camera.main.WorldToScreenPoint(day1.transform.position);

            if (System.Math.Abs(touch.x - day1.transform.position.x) < zone && System.Math.Abs(touch.y - day1.transform.position.y) < zone)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (System.Math.Abs(touch.x - day2.transform.position.x) < zone && System.Math.Abs(touch.y - day2.transform.position.y) < zone)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        vao = true;
        if (col.tag == "Player" && game.chonMission && game.coDien)
        {
            panel.SetActive(true);
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            vao = false;
            panel.SetActive(false);
        }

    }
}