﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class PauseGame : MonoBehaviour
{
    public GameObject pauseMenuUI;
    public GameObject buttonPause;

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1;
        buttonPause.SetActive(true);
    }
    public void ReStart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("GamePlay");
    }
    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0;
        buttonPause.SetActive(false);
    }

}
