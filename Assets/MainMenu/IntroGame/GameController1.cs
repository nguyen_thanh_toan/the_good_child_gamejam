﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController1 : MonoBehaviour
{
    public GameObject bubble;
    public Transform stopMove;
    public GameObject buttonReady;
    private Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    private void Update()
    {
        
        if(transform.position.x - stopMove.position.x <= 1)
        {
            anim.SetBool("Hello", true);
        }
        else{
            transform.position = Vector2.MoveTowards(transform.position, stopMove.position, 7f * Time.deltaTime);
        }
    }
    public void OnBubble()
    {
        bubble.SetActive(true);
        Invoke("OnButtonReady", 10f);
    }

    void OnButtonReady()
    {
        buttonReady.SetActive(true);
    }

    public void Play()
    {
        SceneManager.LoadScene("GamePlay");
    }
}
