﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadyButton : MonoBehaviour
{
    Vector2 vectorMove;
    bool change;
    private void Start()
    {
        InvokeRepeating("ChangeMove", 0, 2f);
    }
    void Update()
    {
        transform.Translate(vectorMove * 0.5f * Time.deltaTime);
    }
    
    void ChangeMove()
    {
        if (change)
            vectorMove = Vector2.left;
        else
            vectorMove = Vector2.right;
        change = !change;
    }
}
